<?php

namespace Drupal\page_title_visibility;

/**
 * Provides an interface for page_title_visibility constants.
 */
interface PageTitleVisibilityInterface {

  const PAGE_DISPLAY_VISIBILITY_PERMISSION = 'administer page display visibility config';
  const PAGE_TITLE_HIDDEN_DESCRIPTION = 'Your account does not have permission to set the page title visibility.';

}
